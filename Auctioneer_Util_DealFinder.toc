## Title: Auctioneer Util: |cff447722Deal Finder
## Notes: Shows deals from recent scan.
##
## Interface: 80300
## LoadOnDemand: 0
## Dependencies: Stubby, Auctioneer
## OptionalDependencies: !nLog, SlideBar, Configator, Babylonian, DebugLib, TipHelper, LibExtraTip, LibDataBroker
## SavedVariables: AuctioneerUtilDealFinderData
##
## Version: <%version%> (<%codename%>)
## Revision: $Id$
## Author: Norganna's AddOns
## X-Part-Of: Auctioneer
## X-Category: Auction House
## X-Max-Interface: 80300
## X-URL: http://auctioneeraddon.com/
## X-Feedback: https://auctioneeraddon.com/slack
##

Main.lua
